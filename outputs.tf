### Global Variables

output "availability_zones" {
  value = "${data.aws_availability_zones.available.names}"
}

output "caller_identity" {
  value = "${data.aws_caller_identity.current.account_id}"
}

output "elb_service_account_arn" {
  value = "${data.aws_elb_service_account.main.arn}"
}

output "cloud_init_ubuntu" {
  value = "${file("${path.module}/cloud-init/ubuntu.yaml")}"
}

output "cloud_init_rhel" {
  value = "${file("${path.module}/cloud-init/rhel.yaml")}"
}

output "cloud_init_bastion_host" {
  value = "${file("${path.module}/cloud-init/bastion-host.yaml")}"
}

output "cloud_init_service_host" {
  value = "${file("${path.module}/cloud-init/service-host.yaml")}"
}

# Latest Ubuntu 18.04 LTS (amd64) 
output "ubuntu_bionic_amd64_ami" {
  value = "${data.aws_ami.ubuntu_bionic_amd64.id}"
}

## Latest Ubuntu 18.04 LTS (arm) 
#output "ubuntu_bionic_arm64_ami" {
#  value = "${data.aws_ami.ubuntu_bionic_arm64.id}"
#}

# Latest Ubuntu 16.04 LTS (amd64) 
output "ubuntu_xenial_amd64_ami" {
  value = "${data.aws_ami.ubuntu_xenial_amd64.id}"
}

# Latest Ubuntu 16.04 LTS (arm) 
#output "ubuntu_xenial_arm64_ami" {
#  value = "${data.aws_ami.ubuntu_xenial_arm64.id}"
#}

output "environments" {
  value = {
    dev     = "dev"
    test    = "test"
    staging = "staging"
    prod    = "prod"
  }
}

output "vpcs" {
  value = {
    dev     = "vpc-dev"
    test    = "vpc-test"
    staging = "vpc-staging"
    prod    = "vpc-prod"
  }
}
