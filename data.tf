## AWS Availability Zones and Region ##

# get all the available availability zones
data "aws_availability_zones" "available" {}

# default region by the webcall
data "aws_region" "current" {}

# get some AWS metadata about our terraform-user account
data "aws_caller_identity" "current" {}

# ELB AWS Account IDs 
# Use this data source to get the Account ID of the AWS Elastic Load Balancing Service Account in a given region for the purpose of whitelisting in S3 bucket policy.
data "aws_elb_service_account" "main" {}

# get lastest ubuntu 18.04 amd64 
data "aws_ami" "ubuntu_bionic_amd64" {
  owners      = ["099720109477"] # canonical
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

# get lastest ubuntu 16.04 amd64 
data "aws_ami" "ubuntu_xenial_amd64" {
  owners      = ["099720109477"] # canonical
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}
